function renderTime() {

	const monthNames = [
		"Jan",
		"Feb",
		"Mar",
		"Apr",
		"May",
		"Jun",
		"Jul",
		"Aug",
		"Sep",
		"Oct",
		"Nov",
		"Dec"
	];

	var currentTime = new Date();


	var month = monthNames[currentTime.getMonth()] ;
	var d = currentTime.getDay();
	var y = currentTime.getFullYear();

	var h = currentTime.getHours();
	var m = currentTime.getMinutes();
	var s = currentTime.getSeconds();
	var diem = "AM";
	
	setTimeout('renderTime()', 1000);

	if (h == 12) {
		h = 0;
	} 

	if (h > 12) {
		h = h - 12;
		diem = "PM";
	} 

	if (h < 10) {
		h = "0" + h;
	}
	if (m < 10) {
			m = "0" + m;
		}
	if (s < 10) {
			s = "0" + s;
		}

	var myClock = document.getElementById('clockDisplay');
	console.log(month);
		myClock.textContent = month + " " + d + " " + y + " " + h + ":" + m + ":" + s + " " + diem;
		myClock.innerText = month + " " + d + " " + y + " " + h + ":" + m + ":" + s + " " + diem;
}

renderTime();